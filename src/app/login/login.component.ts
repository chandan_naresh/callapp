import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  title:string="Login";
  show=false;
  constructor(private userSvc:UserService,private toastCtrl:ToastController,private router:Router) { 

  }

  ngOnInit() {
  }


  async showToast(msg:string){
    let toast=await this.toastCtrl.create({
      message:msg,
      duration:2000,
      position:'middle'
    });
    toast.present();
  }

  doSignUp(email,password){
    debugger;
    console.log(email.value);
    if(email.value && password.value){
      console.log(email.value + " "+ password.value);
      this.userSvc.signup(email.value,password.value).then(r=>{
        console.log('account created successfully' + r);
        this.showToast('Account created successfully').then(r=>{
          this.router.navigateByUrl('');
        })
      },err=>{
        console.log('failed to create account');
        this.showToast('error while creating account');
      })
    }else{
      return;
    }
   
  }

  showLogin(){
    this.title="Login";
    this.show=false;
  }

  showSignUp(){
  this.title="SignUp";
  this.show=true;  
  }

  changeUI(){
    if(this.title=="Login"){
      this.title="SignUp";
    }else{
      this.title="Login"
    }
  }

  doLogin(email,password){
    debugger;
    console.log(email.value);
    if(email.value && password.value){
      console.log(email.value + " "+ password.value);
      this.userSvc.login(email.value,password.value).then(r=>{
        this.showToast('Welcome ..'+ email.value).then(r=>{
          this.router.navigateByUrl('');
        });
      },err=>{
        this.showToast('login failed..check your credentials');
      })
    }else{
      return;
    }
  }

}
