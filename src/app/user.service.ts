import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { Storage } from '@ionic/storage';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  private user:any;
  constructor(public storage: Storage,private afAuth: AngularFireAuth) { 

  }

  authenticated():Promise<any>{
    return new Promise((resolve,reject)=>{
      this.storage.get('user').then(r=>{
        if(r){
        this.user=r;
        resolve();
        }else{
          reject();
        }
      })
    });
  }

  login(email,password):Promise<any>{
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(email,password).then(r=>{
        resolve();
      },err=>{
        reject(err);
      })
    })
    
  }

  listAllUsers(){
    
  }

signup(email,password){
return new Promise((resolve,reject)=>{
  this.afAuth.auth.createUserWithEmailAndPassword(email,password).then(r=>{
    resolve(r);
  },err=>{
    reject(err);
  })
});
  }
}
