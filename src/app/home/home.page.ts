import { Component } from '@angular/core';
import * as AgoraRTC from '../../Agora_Web_SDK/AgoraRTCSDK-2.5.0';

interface Contact{
  name:string;
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  client:any;
  contacts:Contact[]=[];
  constructor(){
    this.contacts.push({
      name:'chandan naresh'
    },{name:'samaya chandan'},{'name':'john doe'},{'name':'tom trovelta'})
    this.client= AgoraRTC.createClient({mode: 'live', codec: "h264"});
  }

  call(contact:Contact){
    //todo: initiate call
    debugger;
    this.client.init('c5a8ac1e84ad45afb1a2cfec8854fc18', (res) =>{
      console.log("AgoraRTC client initialized" + res);
      //init complted...joining the caller to the voice stream
       this.client.join(null,contact.name, contact.name, function(uid) {
        console.log("User " + uid + " join channel successfully");
        let localStream = AgoraRTC.createStream({
          streamID: uid,
          audio: true,
          video: true,
          screen: false}
        );

        localStream.init(function() {
          console.log("getUserMedia successfully");
          localStream.play('agora_local');
          this.client.publish(localStream, function (err) {
            console.log("Publish local stream error: " + err);
          });
          
          this.client.on('stream-published', function (evt) {
            console.log("Publish local stream successfully");
          });
        }, function (err) {
          console.log("getUserMedia failed", err);
        });



      }, function(err) {
        console.log("Join channel failed", err);
      });

    }, function (err) {
      console.log("AgoraRTC client init failed", err);
    });
  }

  recieveCall(evt){
    var stream = evt.stream;
    this.client.on('stream-added', function (evt) {
      
      console.log("New stream added: " + stream.getId());
    
      this.client.subscribe(stream, function (err) {
        console.log("Subscribe stream failed", err);
      });
    });
    this.client.on('stream-subscribed', function (evt) {
      var remoteStream = evt.stream;
      console.log("Subscribe remote stream successfully: " + stream.getId());
      stream.play('agora_remote' + stream.getId());
    })
  }

  
}
